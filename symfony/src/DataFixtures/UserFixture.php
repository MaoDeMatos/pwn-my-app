<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends Fixture {
    public function load(ObjectManager $manager) {
        for ($i = 1; $i <= 20; $i++) {
            $user = new User;
            $user->setUsername('user' . $i);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
