<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController {
  /**
   * @Route("/", name="home")
   */
  public function home() {
    return $this->render('pages/home.html.twig');
  }

  /**
   * @Route("/xss", name="xss")
   */
  public function xss() {
    $name = $_POST['xss'] ?? 'David';

    return $this->render('pages/xss.html.twig', [
      'name' => $name
    ]);
  }

  /**
   * @Route("/csrf", name="csrf")
   */
  public function csrf(Request $request) {
    // Initialize arguments for the template
    $args = ['error' => ''];
    // Check if token exists, write an error if it exists, but is not valid
    $token = $request->request->get('_token') ?? '';

    if (!empty($token)) {
      if ($this->isCsrfTokenValid('csrf-token-test', $token)) {
        $args['error'] = 'Valid CSRF token.';
      } else $args['error'] = 'Invalid CSRF token.';
    }

    return $this->render('pages/csrf.html.twig', $args);
  }

  /**
   * @Route("/sqli", name="sqli")
   */
  public function sqli(Request $req, ManagerRegistry $doctrine) {
    $id = $req->request->get('userid') ?? '1';
    $user = $doctrine->getRepository(User::class)->find($id);

    if (!$user) {
      throw $this->createNotFoundException(
        'No product found for id : ' . $id
      );
    }

    $args = ['users' => [$user]];

    return $this->render('pages/sqli.html.twig', $args);
  }
}
