# Pwn my app

## Introduction

The app has 2 branches : one for the `safe` version of each page, and one for the `vuln`erable version.

Each page integrates its security breach.

See the vulnerabilities [here](doc/vuln.md).

## Start

### Dependencies

- PHP 8
- Composer
- Symfony CLI
- NodeJS

### Commands

Execute these commands in the `./symfony` directory :

<!--
```sh
# Docker
docker-compose up -d
``` -->

```sh
# Manual start
bash install.sh && symfony serve --no-tls
```

When asked if you want to purge, type `yes`.

Or execute the commands in the file `install.sh`, and then use `symfony serve --no-tls` to start the dev server.

## Caveats

The `.env` file is deliberately in the repo to get the app running faster.

The database can be glitched, if it's the case, just delete `symfony/var/data.db` and run `db_set.sh` again.
