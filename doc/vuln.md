# Vulnerabilities

## XSS

Files :

- safe : [xss.html.twig](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/safe/symfony/templates/pages/xss.html.twig)
- vulnerable : [xss.html.twig](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/vuln/symfony/templates/pages/xss.html.twig)

Symfony's templating (Twig) sanitizes all output by default, I had to use the filter `raw` to let a user execute a script trough the form of the page.

Solution : Sanitize !

## CSRF

Files :

- safe :
  - [csrf.html.twig](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/safe/symfony/templates/pages/csrf.html.twig)
  - [BaseController.php](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/safe/symfony/src/Controller/BaseController.php)
- vulnerable :
  - [csrf.html.twig](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/vuln/symfony/templates/pages/csrf.html.twig)
  - [BaseController.php](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/vuln/symfony/src/Controller/BaseController.php)

I did not have time to create a real CSRF breach but I made the token testing on the branch `safe`.

Solution :

Generate a proper form with Symfony Forms, or use a hidden input in the template with a value set to `csrf_token('{name}')` and check its value after getting the form back.

## SQLi

Files :

- safe :
  - [sqli.html.twig](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/safe/symfony/templates/pages/sqli.html.twig)
  - [BaseController.php](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/safe/symfony/src/Controller/BaseController.php)
- vulnerable :
  - [sqli.html.twig](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/vuln/symfony/templates/pages/sqli.html.twig)
  - [BaseController.php](https://gitlab.com/MaoDeMatos/pwn-my-app/-/blob/vuln/symfony/src/Controller/BaseController.php)

You'll get an error if you write an invalid SQL statement (as it searches by id, everything besides valid SQL or simple numbers will throw an error).

Solution :

Use prepared statements, use the ORM (Doctrine for Symfony) to query your database.
